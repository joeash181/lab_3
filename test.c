#include "test.h"


void test_tree(int arr[], int len, int test){
    tree *new = NULL;
    new = init_tree();
    printf("The tree at the start looks like this:\n");
    for (int i = 0; i < len; i++){
        node *new_node = create_node(arr[i]);
        tree_insert(new, new_node);
        printf("%d ", new_node->key);
    }
    printf("\nThe sorted order of the tree is\n");
    inorder_tree_walk(new->root);
    printf("\nThe depth of the tree is %d generations.\n", depth(new->root));
    printf("The size of the tree is %d nodes.\n", size(new->root));
    node *min = tree_minimum(new->root);
    node *max = tree_maximum(new->root);
    printf("The minimum of the tree is %d\n", min->key);
    printf("The maximum of the tree is %d\n", max->key);
    if (tree_search(new, test) != NULL) {
        if (tree_successor(tree_search(new, test)) != NULL) {
            printf("The successor of the node %d is %d\n", test, tree_successor(tree_search(new, test))->key);
        }
        else{
            printf("%d is the node with the highest value.\n", test);
        }
        if (tree_predecessor(tree_search(new, test)) != NULL) {
            printf("The predecessor of the node %d is %d\n", test, tree_predecessor(tree_search(new, test))->key);
        }
        else{
            printf("%d is the node with the lowest value.\n", test);
        }
        node *rem = tree_delete(new, tree_search(new, test));
        free(rem);
        printf("The tree after deleting the node %d, is now:\n", test);
        inorder_tree_walk(new->root);
    }
    else{
        printf("The number you want to test, %d is not in the tree. Try again with another number in the tree!", test);
    }
    free_tree(new);
}