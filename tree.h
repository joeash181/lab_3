//
// Created by Joel on 2019-02-27.
//

#ifndef LAB_3_TREE_H
#define LAB_3_TREE_H
#include <stdio.h>
#include <stdlib.h>
typedef struct node{
    int key;
    struct node *left;
    struct node *right;
    struct node *parent;
}node;

typedef struct tree{
    node *root;
}tree;

tree *init_tree();
node *create_node(int a);
void tree_insert(tree *start, node *a);
node *tree_search(tree *start, int a);
node *tree_minimum(node *a);
node *tree_maximum(node *a);
node *tree_successor(node *a);
node *tree_predecessor(node *a);
void inorder_tree_walk(node *start);
void transplant(tree*start, node *a, node *b);
node *tree_delete(tree *a, node *b);
int depth(node *a);
int size(node *a);
void free_mem(node *a);
void free_tree(tree *a);
#endif //LAB_3_TREE_H
