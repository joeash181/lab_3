#include "main.h"

int main() {
    int first[] = {3, 1, 5, 10, 8, 7};
    printf("Test 1:\n");
    int size_of_first = sizeof(first)/sizeof(int);
    test_tree(first, size_of_first, 7);
    int second[] = {5, 2, 9, 6, 1, 2};
    printf("\nTest 2:\n");
    int size_of_second = sizeof(second)/sizeof(int);
    test_tree(second, size_of_second, 2);
    printf("\nTest 3:\n");
    int third[] = {1, 9, 8, 7, 0, 1, 0, 2};
    int size_of_third = sizeof(third)/sizeof(int);
    test_tree(third, size_of_third, 7);
    return 0;
}