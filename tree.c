#include "tree.h"

tree *init_tree(){
    tree *new = NULL;
    new = malloc(sizeof(tree));
    if (new == NULL){
        printf("Something went wrong with allocating memory!\n");
        exit(1);
    }
    new->root = NULL;
    return new;
}

node *create_node(int key){
    node *new = NULL;
    new = malloc(sizeof(node));
    if (new == NULL){
        printf("Something went wrong with allocating memory!\n");
        exit(1);
    }
    //setting all the pointers to NULL so we can insert it
    new->parent = NULL;
    new->left = NULL;
    new->right = NULL;
    new->key = key;
    return new;
}

void tree_insert(tree *start, node *new_node){
    //Testing for an empty tree
    if (start->root == NULL){
        start->root = new_node;
    }
    else{
        node *temp_parent = start->root;
        node *curr = temp_parent;
        //looping down to the right spot for the new node
        while (curr != NULL){
            temp_parent = curr;
            if (curr->key < new_node->key){
                curr = curr->right;
            }
            else{
                curr = curr->left;
            }
        }
        new_node->parent = temp_parent;
        //giving the node the right parent and fixing the pointers
        if(new_node->key > temp_parent->key){
            temp_parent->right = new_node;
        }
        else{
            temp_parent->left = new_node;
        }
    }
}

node *tree_search(tree *start, int key){
    node *curr = start->root;
    while (curr != NULL && curr->key != key){
        if (curr->key < key){
            curr = curr->right;
        }
        else{
            curr = curr->left;
        }
    }
    return curr;
}

node *tree_minimum(node* start){
    node *curr = start;
    while (curr->left != NULL){
        curr = curr->left;
    }
    return curr;
}

node *tree_maximum(node* start){
    node *curr = start;
    while (curr->right != NULL){
        curr = curr->right;
    }
    return curr;
}

node *tree_successor(node *next){
    node *curr = next;
    //testing for a right child and if one is present finding the minimum in that sub tree
    if (curr->right != NULL){
        return tree_minimum(curr->right);
    }
    node *parent = next->parent;
    //Trying to find the first node among ancestors of which the line is a left child
    while (parent != NULL && curr == parent->right){
        curr = parent;
        parent = parent->parent;
    }
    return parent;
}

//same as successor just opposite
node *tree_predecessor(node *next){
    node *curr = next;
    if (curr->left != NULL){
        return tree_maximum(curr->left);
    }
    node *parent = next->parent;
    while (parent != NULL && curr == parent->left){
        curr = parent;
        parent = parent->parent;
    }
    return parent;
}

//recursively printing all the numbers in the right order by finding the left most node at all points
void inorder_tree_walk(node* start){
    if (start == NULL){
        return;
    }
    inorder_tree_walk(start->left);
    printf("%d ", start->key);
    inorder_tree_walk(start->right);
}

// changing nodes by giving the new node the correct parent pointers both to and from
void transplant(tree *start, node *remove, node *replace){
    if (remove == start->root){
        start->root = replace;
    }
    else if(remove == remove->parent->left){
        remove->parent->left = replace;
    }
    else{
        remove->parent->right = replace;
    }
    if(replace != NULL){
        replace->parent = remove->parent;
    }
}


node *tree_delete(tree *start, node *to_remove){
    if(to_remove != NULL) {
        if (to_remove->left == NULL) {
            //replacing the node with it's right child if there is no left child
            transplant(start, to_remove, to_remove->right);
        }
        else if (to_remove->right == NULL) {
            //replacing the node with it's left child if there is no right child
            transplant(start, to_remove, to_remove->left);
        }
        else {
            //finding the successor of the to_replace node;
            node *replace = tree_minimum(to_remove->right);
            if (replace->parent != to_remove) {
                //giving the new node the old nodes right child
                transplant(start, replace, replace->right);
                replace->right = to_remove->right;
                replace->right->parent = replace;
            }
            //giving the new node the old nodes left child
            transplant(start, to_remove, replace);
            replace->left = to_remove->left;
            replace->left->parent = replace;
        }
        //setting the old nodes pointers to NULL
        to_remove->parent = NULL;
        to_remove->left = NULL;
        to_remove->right = NULL;
        return to_remove;
    }
    else{
        //printing error if no such node exists
        printf("There is no such node in the tree!\n");
        return NULL;
    }
}

//recursively calculating the depth of the tree at it's deepest
int depth(node *root){
    if (root == NULL){
        return 0;
    }
    int left_depth = depth(root->left);
    int right_depth = depth(root->right);
    if(left_depth > right_depth){
        return left_depth+ 1;
    }
    return right_depth + 1;
}

//recursively calculating the size of the tree
int size(node *root){
    if (root == NULL){
        return 0;
    }
    int right = size(root->right);
    int left = size(root->left);
    return 1 + left + right;
}

//freeing up all the memory that was allocated
void free_mem(node *start){
    if (start == NULL){
        return;
    }
    start->parent = NULL;
    free_mem(start->left);
    start->left = NULL;
    free_mem(start->right);
    start->right = NULL;
    free(start);
}

void free_tree(tree *start){
    free_mem(start->root);
    free(start);
}